CREATE TABLE public.SHIP
(
	ID      					BIGSERIAL 	PRIMARY KEY,
	NAME    					VARCHAR 	NOT NULL,
	MAX_NUMBER_OF_PASSENGERS 	int         NOT NULL,
	MAX_NUMBER_OF_VEHICLES	 	int         NOT NULL
);

ALTER SEQUENCE SHIP_ID_seq RESTART 100000 INCREMENT BY 50;

CREATE TABLE public.ROUTE
(
	ID      				BIGSERIAL 			PRIMARY KEY,
	NUMBER_OF_PASSENGERS 	int,
	NUMBER_OF_VEHICLES	 	int,
	
	PORT_OF_DEPARTURE		VARCHAR				NOT NULL,
	EXPECTED_DEPARTURE_TIME	TIMESTAMP,
	ACTUAL_DEPARTURE_TIME	TIMESTAMP,
	
	PORT_OF_ARRIVAL			VARCHAR				NOT NULL,
	EXPECTED_ARRIVAL_TIME	TIMESTAMP,
	ARRIVAL_DEPARTURE_TIME	TIMESTAMP,
	
	PRICE       			DOUBLE PRECISION,	
	
	SHIP_ID   				BIGINT 		        NOT NULL REFERENCES SHIP(ID)

);

ALTER SEQUENCE ROUTE_ID_seq RESTART 100000 INCREMENT BY 50;