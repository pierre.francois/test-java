package com.po.freight.ship;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.po.freight.ship.domain.Route;

@RestController
public class ShipAPI {
	
	private ShipService shipService;
	
	@Autowired
	public ShipAPI(ShipService shipService) {
		
		this.shipService = shipService;
	}
	
	@GetMapping
	@RequestMapping(value = "/routes", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Route> findAllRoutes() {
		
		return shipService.findAllRoutes();
	}
}
