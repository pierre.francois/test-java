package com.po.freight.ship;

import java.util.List;

import com.po.freight.ship.domain.Route;

public interface ShipService {

	List<Route> findAllRoutes();
}
