package com.po.freight.ship.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SHIP")
public class Ship {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SHIP_ID_seq")
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "MAX_NUMBER_OF_PASSENGERS")
	private int maxNumberOfPassengers;
	
	@Column(name = "MAX_NUMBER_OF_VEHICLES")
	private int maxNumberOfVehicles;
	
	@OneToMany(mappedBy = "ship", cascade = CascadeType.ALL)
	private List<Route> routes;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ship other = (Ship) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
