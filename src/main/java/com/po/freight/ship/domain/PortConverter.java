package com.po.freight.ship.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class PortConverter implements AttributeConverter<Port, String> {

	@Override
	public String convertToDatabaseColumn(Port port) {
		
		return port.toString();
	}

	@Override
	public Port convertToEntityAttribute(String port) {
		
		return Port.valueOf(port);
	}

}
