package com.po.freight.ship.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.po.freight.booking.domain.Leg;
import com.po.freight.booking.domain.LegStatus;

@Entity
@Table(name = "ROUTE")
public class Route {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROUTE_ID_seq")
	private Long id;
	
	@Column(name = "NUMBER_OF_PASSENGERS")
	private int numberOfPassengers;
	
	@Column(name = "NUMBER_OF_VEHICLES")
	private int numberOfVehicles;
	
	@Column(name = "PORT_OF_DEPARTURE")
	@Convert(converter = PortConverter.class)
	private Port from;

	@Column(name = "EXPECTED_DEPARTURE_TIME")
	@JsonSerialize(using = DateJsonSerializer.class)
	@JsonDeserialize(using = DateJsonDeserializer.class)
	private LocalDateTime expectedDepartureTime;
	
	@Column(name = "ACTUAL_DEPARTURE_TIME")
	@JsonSerialize(using = DateJsonSerializer.class)
	@JsonDeserialize(using = DateJsonDeserializer.class)
	private LocalDateTime actualDepartureTime;

	@Column(name = "PORT_OF_ARRIVAL")
	@Convert(converter = PortConverter.class)
	private Port to;

	@Column(name = "EXPECTED_ARRIVAL_TIME")
	@JsonSerialize(using = DateJsonSerializer.class)
	@JsonDeserialize(using = DateJsonDeserializer.class)
	private LocalDateTime expectedArrivalTime;
	
	@Column(name = "ARRIVAL_DEPARTURE_TIME")
	@JsonSerialize(using = DateJsonSerializer.class)
	@JsonDeserialize(using = DateJsonDeserializer.class)
	private LocalDateTime actualArrivalTime;
	
	@Column(name = "PRICE")
	private Float price;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Ship ship;
	
	@OneToMany(mappedBy = "route", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Leg> legs;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

	public int getNumberOfVehicles() {
		return numberOfVehicles;
	}

	public void setNumberOfVehicles(int numberOfVehicles) {
		this.numberOfVehicles = numberOfVehicles;
	}

	public Port getFrom() {
		return from;
	}

	public void setFrom(Port from) {
		this.from = from;
	}

	public LocalDateTime getExpectedDepartureTime() {
		return expectedDepartureTime;
	}

	public void setExpectedDepartureTime(LocalDateTime expectedDepartureTime) {
		this.expectedDepartureTime = expectedDepartureTime;
	}

	public LocalDateTime getActualDepartureTime() {
		return actualDepartureTime;
	}

	public void setActualDepartureTime(LocalDateTime actualDepartureTime) {
		this.actualDepartureTime = actualDepartureTime;
	}

	public Port getTo() {
		return to;
	}

	public void setTo(Port to) {
		this.to = to;
	}

	public LocalDateTime getExpectedArrivalTime() {
		return expectedArrivalTime;
	}

	public void setExpectedArrivalTime(LocalDateTime expectedArrivalTime) {
		this.expectedArrivalTime = expectedArrivalTime;
	}

	public LocalDateTime getActualArrivalTime() {
		return actualArrivalTime;
	}

	public void setActualArrivalTime(LocalDateTime actualArrivalTime) {
		this.actualArrivalTime = actualArrivalTime;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public List<Leg> getLegs() {
		return legs;
	}

	public void setLegs(List<Leg> legs) {
		this.legs = legs;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Route other = (Route) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public static Builder buildWith() {
		
		return new Builder();
	}
	
	public static class Builder {
		
		private Route route;
		
		public Builder() {
			
			route = new Route();
		}
		
		public Builder id(long id) {
			
			route.setId(id);
			
			return this;
		}
		
		public Builder numberOfPassengers(int numberOfPassengers) {
			
			route.setNumberOfPassengers(numberOfPassengers);
						
			return this;		
		}
		
		public Builder numberOfVehicles(int numberOfVehicles) {
			
			route.setNumberOfVehicles(numberOfVehicles);
			
			return this;		
		}
		
		public Builder from(Port from) {
			
			route.setFrom(from);
			
			return this;		
		}
		
		public Builder expectedDepartureTime(LocalDateTime expectedDepartureTime) {
			
			route.setExpectedDepartureTime(expectedDepartureTime);
			
			return this;		
		}
		
		public Builder to(Port to) {
			
			route.setTo(to);
			
			return this;		
		}
		
		public Builder expectedArrivalTime(LocalDateTime expectedArrivalTime) {
			
			route.setExpectedArrivalTime(expectedArrivalTime);
			
			return this;		
		}
		
		public Builder price(Float price) {
			
			route.setPrice(price);
			
			return this;		
		}
		
		public Route instance() {
			
			return route;
		}
		
	}
}
