package com.po.freight.ship.domain;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class DateJsonSerializer extends JsonSerializer<LocalDateTime>{

	@Override
	public void serialize(LocalDateTime date, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		gen.writeString(date.format(dateTimeFormatter));		
	}

}
