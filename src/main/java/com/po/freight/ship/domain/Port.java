package com.po.freight.ship.domain;

public enum Port {
	DOVER, CALAIS;
}
