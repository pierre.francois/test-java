package com.po.freight.ship.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.po.freight.ship.ShipRepository;
import com.po.freight.ship.domain.Route;

@Repository
public class ShipRepositoryJPA implements ShipRepository {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Route> findAllRoutes() {
		
		return entityManager.createQuery("select r from Route r order by r.from, r.expectedDepartureTime").getResultList();
	}
}
