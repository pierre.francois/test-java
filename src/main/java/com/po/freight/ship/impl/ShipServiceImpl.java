package com.po.freight.ship.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.po.freight.ship.ShipRepository;
import com.po.freight.ship.ShipService;
import com.po.freight.ship.domain.Route;

@Service
public class ShipServiceImpl implements ShipService {
	
	private ShipRepository shipRepository;
	
	@Autowired
	public ShipServiceImpl(ShipRepository shipRepository) {
		this.shipRepository = shipRepository;
	}

	@Override
	public List<Route> findAllRoutes() {
		return shipRepository.findAllRoutes();
	}
}
