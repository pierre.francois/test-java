package com.po.freight.ship;

import java.util.List;

import com.po.freight.ship.domain.Route;

public interface ShipRepository {

	/**
	 * @return routes grouped by departure port then time.
	 */
	List<Route> findAllRoutes();
}
