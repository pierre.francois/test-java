package com.po.freight.payment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

/**
 * @author takima
 * 
 * The payment API via PayPal is made simple on teaching purpose.
 *
 */
@Service
public class PayPalPayment {

	@Autowired
	APIContext context;
	
	@Autowired
	public PayPalPayment() {}
	
	public Payment createPayment(float cost) throws PayPalRESTException {
		
		Payment payment = new Payment();
		Amount amount = new Amount();
		amount.setTotal(new Float(cost).toString());
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		
		List<Transaction> transactions = new ArrayList<>();
		transactions.add(transaction);
		
		payment.setTransactions(transactions);
		
		return payment.create(context);
	}
	
	public Payment executePayment(String paymentId, String payerId) throws PayPalRESTException {
		
		Payment payment = new Payment();
		payment.setId(paymentId);
		PaymentExecution paymentExecute = new PaymentExecution();
		paymentExecute.setPayerId(payerId);
		
		return payment.execute(context, paymentExecute);
	}
}
