package com.po.freight.booking;

import com.po.freight.booking.domain.Discount;

public interface DiscountRepository {

    Discount findDiscount(long id);
}
