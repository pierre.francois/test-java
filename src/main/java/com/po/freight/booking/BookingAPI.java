package com.po.freight.booking;

import com.po.freight.booking.domain.Voyage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookingAPI {
	
	private BookingService bookingService;
	
	@Autowired
	public BookingAPI(BookingService bookingService) {
		
		this.bookingService = bookingService;
	}

	@PostMapping(value = "/booking", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Voyage createBooking(@RequestBody BookingCreationReq bookingRequest) throws Exception {
			Voyage voyage = bookingService.createBooking(bookingRequest);
			return voyage;
	}
}
