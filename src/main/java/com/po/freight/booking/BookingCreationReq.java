package com.po.freight.booking;

import java.util.List;

import com.po.freight.booking.domain.Customer;
import com.po.freight.booking.domain.Discount;
import com.po.freight.ship.domain.Route;

public class BookingCreationReq {	
	
	private Customer customer;	
	
	private List<Route> chosenRoutes;
	
	private Discount discount;
	
	public BookingCreationReq(Customer customer, List<Route> chosenRoutes, Discount discount) {
		this.customer = customer;
		this.chosenRoutes = chosenRoutes;
		this.discount = discount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Route> getChosenRoutes() {
		return chosenRoutes;
	}

	public void setChosenRoutes(List<Route> chosenRoutes) {
		this.chosenRoutes = chosenRoutes;
	}

	public Discount getDiscount() {
		return discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}	
}
