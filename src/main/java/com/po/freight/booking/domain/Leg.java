package com.po.freight.booking.domain;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.po.freight.ship.domain.Route;

@Entity
@Table(name = "LEG")
public class Leg {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LEG_ID_seq")
    private Long id;
	
	@Column(name = "STATUS")
	@Convert(converter = LegStatusConverter.class)
	private LegStatus status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VOYAGE_ID")
	@JsonIgnore
	private Voyage voyage;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROUTE_ID")
	private Route route;

	public Leg(Voyage voyage, Route route) {
		this.voyage = voyage;
		this.route = route;
		this.status = LegStatus.BOOKED;
	}

	public Leg() {

	}


	public void setStatus(LegStatus status) {
		this.status = status;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}	
	
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Leg other = (Leg) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	public static Builder buildWith() {

		return new Builder();
	}
	
	public static class Builder {
		
		private Leg leg;
		
		public Builder() {
		
			leg = new Leg();
		}
		
		public Builder status(LegStatus status) {
			
			leg.setStatus(status);
			
			return this;
		}
		
		public Builder route(Route route) {
			
			leg.setRoute(route);
			
			return this;
		}
		
		public Builder voyage(Voyage voyage) {
			
			leg.setVoyage(voyage);
			
			return this;
		}
		
		public Leg instance() {
			
			return leg;
		}
	}
}
