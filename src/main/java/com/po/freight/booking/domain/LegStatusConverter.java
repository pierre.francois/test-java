package com.po.freight.booking.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LegStatusConverter implements AttributeConverter<LegStatus, String> {

	@Override
	public String convertToDatabaseColumn(LegStatus status) {
		
		return status.toString();
	}

	@Override
	public LegStatus convertToEntityAttribute(String status) {
		
		return LegStatus.valueOf(status);
	}

}
