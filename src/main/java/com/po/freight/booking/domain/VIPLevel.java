package com.po.freight.booking.domain;

public enum VIPLevel {

	MOSS(0f, 2), OFFICER(0.02f, 2), CAPTAIN(0.05f, 20);
	
	private Float discountRate;
	
	private Integer maxNumberOfLegsInOneVoyage;
	
	private VIPLevel(Float discountRate, Integer maxNumberOfLegsInOneVoyage) {
		
		this.discountRate = discountRate;
		this.maxNumberOfLegsInOneVoyage = maxNumberOfLegsInOneVoyage;
	}
	
	public Float discountRate() {
		
		return discountRate;
	}
	
	public Integer maxNumberOfLegsInOneVoyage() {
		
		return maxNumberOfLegsInOneVoyage;
	}
}
