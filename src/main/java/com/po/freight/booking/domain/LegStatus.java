package com.po.freight.booking.domain;

public enum LegStatus {
	
	BOOKED, CHECKED_IN, BOARDED, DISEMBARKED, CANCELED, REFUNDED
}
