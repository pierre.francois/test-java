package com.po.freight.booking.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

@Entity
@Table(name = "VOYAGE")
public class Voyage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VOYAGE_ID_seq")
	private Long id;
	
	@OneToMany(mappedBy = "voyage", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Leg> legs;
	
	@OneToOne (mappedBy = "voyage", cascade = CascadeType.ALL)
	private Discount discount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CUSTOMER_ID")
	@JsonIgnore
	private Customer customer;

	public Voyage() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Leg> getLegs() {
		return legs;
	}

	public void setLegs(List<Leg> legs) {
		this.legs = legs;
	}

	public Discount getDiscount() {
		return discount;
	}

	@Transactional
	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	public Customer getCustomer() {
		return customer;
	}

	@Transactional
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void add(Leg leg) {
		
		if (legs == null)
			legs = new ArrayList<>();
		
		leg.setVoyage(this);
		legs.add(leg);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voyage other = (Voyage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
	
}
