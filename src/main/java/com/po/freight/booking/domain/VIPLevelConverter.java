package com.po.freight.booking.domain;

import javax.persistence.AttributeConverter;

public class VIPLevelConverter implements AttributeConverter<VIPLevel, String>{

	@Override
	public String convertToDatabaseColumn(VIPLevel level) {
		
		return level.toString();
	}

	@Override
	public VIPLevel convertToEntityAttribute(String level) {
		
		return VIPLevel.valueOf(level);
	}	
}
