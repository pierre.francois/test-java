package com.po.freight.booking;

import com.paypal.base.rest.PayPalRESTException;
import com.po.freight.booking.exceptions.NotEnoughTimeBeforeDepartureException;
import com.po.freight.booking.exceptions.TooMuchLegsBookedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class BookingExceptionHandler {

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Object> runtimeException(RuntimeException exception, WebRequest request) {

		return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({TooMuchLegsBookedException.class , NotEnoughTimeBeforeDepartureException.class})
	public ResponseEntity<Object> bookingException(Exception exception, WebRequest request) {

		return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.BAD_REQUEST);
	}

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Object> nullPointerException(RuntimeException exception, WebRequest request) {

        return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

	@ExceptionHandler(PayPalRESTException.class)
	public ResponseEntity<Object> payPalRESTException(PayPalRESTException exception, WebRequest request) {

		return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
