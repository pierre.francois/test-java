package com.po.freight.booking;

import com.po.freight.booking.domain.Discount;

public interface DiscountService {
    Discount findDiscount(long id);
}
