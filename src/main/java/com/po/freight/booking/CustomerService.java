package com.po.freight.booking;

import com.po.freight.booking.domain.Customer;

public interface CustomerService {

    Customer findCustomer(long id);
}
