package com.po.freight.booking;

import com.po.freight.booking.domain.Voyage;

public interface BookingService {

    Voyage createBooking(BookingCreationReq bookingCreationReq) throws Exception;
}
