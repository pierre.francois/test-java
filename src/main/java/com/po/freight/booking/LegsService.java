package com.po.freight.booking;

import com.po.freight.booking.domain.Leg;

public interface LegsService {

    Leg createLegs(Leg leg);
}
