package com.po.freight.booking.exceptions;

public class TooMuchLegsBookedException extends Exception{

    public TooMuchLegsBookedException() {
    }

    public TooMuchLegsBookedException(String message) {
        super(message);
    }

    public TooMuchLegsBookedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooMuchLegsBookedException(Throwable cause) {
        super(cause);
    }

    public TooMuchLegsBookedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
