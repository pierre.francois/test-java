package com.po.freight.booking.exceptions;

public class NotEnoughTimeBeforeDepartureException extends Exception {
    public NotEnoughTimeBeforeDepartureException() {
    }

    public NotEnoughTimeBeforeDepartureException(String message) {
        super(message);
    }

    public NotEnoughTimeBeforeDepartureException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotEnoughTimeBeforeDepartureException(Throwable cause) {
        super(cause);
    }

    public NotEnoughTimeBeforeDepartureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
