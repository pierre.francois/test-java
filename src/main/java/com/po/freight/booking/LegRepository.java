package com.po.freight.booking;

import com.po.freight.booking.domain.Leg;

public interface LegRepository {

    Leg createLeg(Leg leg);
}
