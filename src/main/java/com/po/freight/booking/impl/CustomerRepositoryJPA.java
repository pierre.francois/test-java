package com.po.freight.booking.impl;

import com.po.freight.booking.CustomerRepository;
import com.po.freight.booking.domain.Customer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CustomerRepositoryJPA implements CustomerRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Customer getCustomer(long id) {
        return entityManager.find(Customer.class, id);
    }
}
