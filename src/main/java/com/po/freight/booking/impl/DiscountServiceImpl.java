package com.po.freight.booking.impl;

import com.po.freight.booking.DiscountRepository;
import com.po.freight.booking.DiscountService;
import com.po.freight.booking.domain.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscountServiceImpl implements DiscountService {

    private final DiscountRepository discountRepository;

    @Autowired
    public DiscountServiceImpl(DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    @Override
    public Discount findDiscount(long id) {
        return discountRepository.findDiscount(id);
    }
}
