package com.po.freight.booking.impl;

import com.po.freight.booking.VoyageRepository;
import com.po.freight.booking.VoyageService;
import com.po.freight.booking.domain.Voyage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoyageServiceImpl implements VoyageService {

    private final VoyageRepository voyageRepository;

    @Autowired
    public VoyageServiceImpl(VoyageRepository voyageService) {
        this.voyageRepository = voyageService;
    }

    @Override
    public Voyage createVoyage(Voyage voyage) {
        return voyageRepository.createVoyage(voyage);
    }
}
