package com.po.freight.booking.impl;

import com.po.freight.booking.*;
import com.po.freight.booking.domain.*;
import com.po.freight.booking.exceptions.NotEnoughTimeBeforeDepartureException;
import com.po.freight.booking.exceptions.TooMuchLegsBookedException;
import com.po.freight.payment.PayPalPayment;
import com.po.freight.ship.domain.Route;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
@Slf4j
public class BookingServiceImpl implements BookingService {

	private final BookingRepository bookingRepository;
	private final LegRepository legRepository;
	private final PayPalPayment payPalPayment;
	private final VoyageRepository voyageRepository;
	private final DiscountService discountService;

	@Autowired
	public BookingServiceImpl(BookingRepository bookingRepository, PayPalPayment payPalPayment, LegRepository legRepository, VoyageRepository voyageRepository, DiscountService discountService) {
		this.bookingRepository = bookingRepository;
		this.payPalPayment = payPalPayment;
		this.legRepository = legRepository;
		this.voyageRepository = voyageRepository;
		this.discountService = discountService;
	}

	@Override
	public Voyage createBooking(BookingCreationReq bookingCreationReq) throws Exception {
		if(bookingCreationReq.getChosenRoutes() == null ) throw new NullPointerException("Route not specified");
		if(!bookingCreationReq.getCustomer().getLevel().equals(VIPLevel.CAPTAIN)
				&& bookingCreationReq.getChosenRoutes().size()>2)
			throw new TooMuchLegsBookedException("You cannot book more than two legs at a time");

		Voyage voyage = new Voyage();
		voyage.setCustomer(bookingCreationReq.getCustomer());

		voyage.setDiscount(bookingCreationReq.getDiscount());

		voyage = voyageRepository.createVoyage(voyage);
		LocalDateTime time = LocalDateTime.now().plusHours(12);
		for(Route route : bookingCreationReq.getChosenRoutes()){
			if(route.getExpectedDepartureTime().isBefore(time))
				throw new NotEnoughTimeBeforeDepartureException("You cannot choose this leg, departure is too early");
			legRepository.createLeg(new Leg(voyage, route));
		}

		return voyage;
	}


}
