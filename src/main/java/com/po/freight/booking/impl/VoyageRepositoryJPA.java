package com.po.freight.booking.impl;

import com.po.freight.booking.VoyageRepository;
import com.po.freight.booking.domain.Voyage;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class VoyageRepositoryJPA implements VoyageRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Voyage createVoyage(Voyage voyage) {
        return entityManager.merge(voyage);
    }
}
