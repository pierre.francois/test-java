package com.po.freight.booking.impl;

import com.po.freight.booking.DiscountRepository;
import com.po.freight.booking.domain.Discount;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class DiscountServiceJPA implements DiscountRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Discount findDiscount(long id) {
        return entityManager.find(Discount.class, id);
    }
}
