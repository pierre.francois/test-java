package com.po.freight.booking.impl;

import com.po.freight.booking.LegRepository;
import com.po.freight.booking.domain.Leg;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class LegRepositoryJPA implements LegRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Leg createLeg(Leg leg) {
        return entityManager.merge(leg);
    }
}
