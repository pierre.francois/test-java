package com.po.freight.booking;

import com.po.freight.booking.domain.Customer;

public interface CustomerRepository {

    Customer getCustomer(long id);
}
