package com.po.freight;

import java.util.HashMap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Configuration
public class FreightBusinessConfig {

	@Bean
	public APIContext apiContext() throws PayPalRESTException {
		APIContext context = new APIContext("acessToken");
		context.setConfigurationMap(new HashMap<>());
		return context;
	}
}
