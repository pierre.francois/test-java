INSERT INTO SHIP (ID, NAME, MAX_NUMBER_OF_PASSENGERS, MAX_NUMBER_OF_VEHICLES) VALUES (1, 'Das Boot', 230, 25);
INSERT INTO SHIP (ID, NAME, MAX_NUMBER_OF_PASSENGERS, MAX_NUMBER_OF_VEHICLES) VALUES (2, 'Queen Anne''s Revenge', 520, 45);
INSERT INTO SHIP (ID, NAME, MAX_NUMBER_OF_PASSENGERS, MAX_NUMBER_OF_VEHICLES) VALUES (3, 'Arudj', 230, 25);

INSERT INTO ROUTE (ID,  NUMBER_OF_PASSENGERS, NUMBER_OF_VEHICLES, PORT_OF_DEPARTURE, EXPECTED_DEPARTURE_TIME, PORT_OF_ARRIVAL, EXPECTED_ARRIVAL_TIME, PRICE, SHIP_ID)
	VALUES (1, 0, 0, 'CALAIS', '2021/01/15 12:50:00', 'DOVER', '2021/01/15 13:20:00', 125, 1);
INSERT INTO ROUTE (ID,  NUMBER_OF_PASSENGERS, NUMBER_OF_VEHICLES, PORT_OF_DEPARTURE, EXPECTED_DEPARTURE_TIME, PORT_OF_ARRIVAL, EXPECTED_ARRIVAL_TIME, PRICE, SHIP_ID)
	VALUES (2, 0, 0, 'CALAIS', '2021/01/15 10:10:00', 'DOVER', '2021/01/15 10:40:00', 225, 2);
INSERT INTO ROUTE (ID,  NUMBER_OF_PASSENGERS, NUMBER_OF_VEHICLES, PORT_OF_DEPARTURE, EXPECTED_DEPARTURE_TIME, PORT_OF_ARRIVAL, EXPECTED_ARRIVAL_TIME, PRICE, SHIP_ID)
	VALUES (3, 0, 0, 'CALAIS', '2021/01/15 15:15:00', 'DOVER', '2021/01/15 15:45:00', 150, 3);
INSERT INTO ROUTE (ID,  NUMBER_OF_PASSENGERS, NUMBER_OF_VEHICLES, PORT_OF_DEPARTURE, EXPECTED_DEPARTURE_TIME, PORT_OF_ARRIVAL, EXPECTED_ARRIVAL_TIME, PRICE, SHIP_ID)
	VALUES (4, 0, 0, 'DOVER', '2021/01/15 16:00:00', 'CALAIS', '2021/01/15 18:30:00', 175, 1);
INSERT INTO ROUTE (ID,  NUMBER_OF_PASSENGERS, NUMBER_OF_VEHICLES, PORT_OF_DEPARTURE, EXPECTED_DEPARTURE_TIME, PORT_OF_ARRIVAL, EXPECTED_ARRIVAL_TIME, PRICE, SHIP_ID)
	VALUES (5, 0, 0, 'DOVER', '2021/01/15 12:10:00', 'CALAIS', '2021/01/15 14:40:00', 200, 2);
INSERT INTO ROUTE (ID,  NUMBER_OF_PASSENGERS, NUMBER_OF_VEHICLES, PORT_OF_DEPARTURE, EXPECTED_DEPARTURE_TIME, PORT_OF_ARRIVAL, EXPECTED_ARRIVAL_TIME, PRICE, SHIP_ID)
	VALUES (6, 0, 0, 'DOVER', '2021/01/15 17:30:00', 'CALAIS', '2021/01/15 20:00:00', 160, 3);