package com.po.freight.booking;

import com.po.freight.booking.domain.Customer;
import com.po.freight.booking.domain.Discount;
import com.po.freight.ship.ShipService;
import com.po.freight.ship.domain.Route;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
@Sql("/sql/booking-data.sql")
@Transactional @Rollback
class BookingAPITest {

    @Autowired CustomerService customerService;
    @Autowired ShipService shipService;
    @Autowired DiscountService discountService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void bookingCreation_noCustomerValue_notFound() throws Exception {
        List<Route> choseRoute = shipService.findAllRoutes().subList(0,1);
        Discount discount = discountService.findDiscount(1L);

        BookingCreationReq book =  new BookingCreationReq(null, choseRoute, discount);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(new ObjectMapper().writeValueAsString(book))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void bookingCreation_noRouteValue_notFound() throws Exception {
        Customer customer = customerService.findCustomer(1L);
        Discount discount = discountService.findDiscount(1L);

        BookingCreationReq book =  new BookingCreationReq(customer, null, discount);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(new ObjectMapper().writeValueAsString(book))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void bookingCreation_oneLeg_NoDiscount_MossLevel_createBooking() throws Exception {
        Customer customer = customerService.findCustomer(1L);
        List<Route> choseRoute =  Arrays.asList(shipService.findAllRoutes().get(0));

        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(new ObjectMapper().writeValueAsString(book))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void bookingCreation_twoLegs_NoDiscount_MossLevel_createBooking() throws Exception {
        Customer customer = customerService.findCustomer(1L);
        List<Route> choseRoute = shipService.findAllRoutes().subList(0,2);

        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(new ObjectMapper().writeValueAsString(book))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void bookingCreation_twoLegs_WithDiscount_MossLevel_createBooking() throws Exception {
        Customer customer = customerService.findCustomer(1L);
        List<Route> choseRoute = shipService.findAllRoutes().subList(0,2);
        Discount discount = discountService.findDiscount(1L);

        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, discount);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(new ObjectMapper().writeValueAsString(book))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void bookingCreation_moreThanTwoLegs_WithDiscount_MossLevel_forbidden() throws Exception {
        Customer customer = customerService.findCustomer(1L);
        List<Route> choseRoute = shipService.findAllRoutes();
        Discount discount = discountService.findDiscount(1L);

        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, discount);
        String obj = new ObjectMapper().writeValueAsString(book);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(obj)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void bookingCreation_oneLeg_TooEarly_MossLevel_forbidden() throws Exception {
        Customer customer = customerService.findCustomer(1L);
        List<Route> choseRoute = shipService.findAllRoutes().subList(0,2);
        choseRoute.get(0).setExpectedDepartureTime(LocalDateTime.now());

        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
                .content(new ObjectMapper().writeValueAsString(book))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

//    //TODO add fonctionnalité pour recalculer/ajouter une discount
//    @Test
//    void bookingCreation_oneLeg_NoDiscount_OfficerLevel_createBooking() throws Exception{
//        Customer customer = customerService.findCustomer(2L);
//        List<Route> choseRoute = shipService.findAllRoutes().subList(0,1);
//
//        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
//        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
//                .content(new ObjectMapper().writeValueAsString(book))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(201));
//    }
//
//    @Test
//    void bookingCreation_oneLeg_NoDiscount_CaptainLevel_createBooking() throws Exception{
//        Customer customer = customerService.findCustomer(3L);
//        List<Route> choseRoute = shipService.findAllRoutes().subList(0,1);
//
//        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
//        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
//                .content(new ObjectMapper().writeValueAsString(book))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(201));
//    }
//
//    @Test
//    void bookingCreation_twoLegs_NoDiscount_OfficerLevel_createBooking() throws Exception {
//        Customer customer = customerService.findCustomer(2L);
//        List<Route> choseRoute = shipService.findAllRoutes().subList(0,2);
//
//        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
//        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
//                .content(new ObjectMapper().writeValueAsString(book))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(201));
//    }
//
//
//    @Test
//    void bookingCreation_twoLegs_NoDiscount_CaptainLevel_createBooking() throws Exception {
//        Customer customer = customerService.findCustomer(3L);
//        List<Route> choseRoute = shipService.findAllRoutes().subList(0,2);
//
//        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, null);
//        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
//                .content(new ObjectMapper().writeValueAsString(book))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(201));
//    }
//
//    //TODO add fonctionnalité pour recalculer/ajouter une discount
//    @Test
//    void bookingCreation_moreThanTwoLegs_OfficerLevel_forbidden() throws Exception {
//        Customer customer = customerService.findCustomer(2L);
//        List<Route> choseRoute = shipService.findAllRoutes();
//        Discount discount = discountService.findDiscount(1L);
//
//        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, discount);
//        String obj = new ObjectMapper().writeValueAsString(book);
//        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
//                .content(obj)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(403));
//    }
//
//    @Test
//    void bookingCreation_moreThanTwoLegs_CaptainLevel_createBooking() throws Exception {
//        //TODO add conditionfor captain
//        Customer customer = customerService.findCustomer(3L);
//        List<Route> choseRoute = shipService.findAllRoutes();
//        Discount discount = discountService.findDiscount(1L);
//
//        BookingCreationReq book =  new BookingCreationReq(customer, choseRoute, discount);
//        mockMvc.perform(MockMvcRequestBuilders.post("/booking")
//                .content(new ObjectMapper().writeValueAsString(book))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(201));
//    }
}