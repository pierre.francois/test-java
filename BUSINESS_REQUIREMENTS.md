# Business requirements

This document presents P&O freight business requirements. 

## General

P&O has 3 ships that carry vehicles and passengers from Dover to Calais and conversely.

A ship has an expected departure and arrival date and time. 

An actual departure time is set after the ship is allowed to leave. In the same way, an actual arrival time is set when a ship reaches its destination.

## Performance

A web page is displayed in 1 s.

A booking creation and payment do not take more than 5 s.

## Booking

### Voyage booking

Customers can book via our site.

Routes are presented ordered by departure port then departure time.

A customer can book a voyage containing one or two legs (at least one) :

- He choses one or two legs.
- Then, he books them. Payment is done at this time via the PayPal payment service.

**Business rule 1 :** a customer can book legs which departure time 12 hours after present.  

### Booking status

A booking can have the following status :

- Booked : customer has booked and paid a voyage.
- Checked-in : vehicle and passenger have registered via a vending machine or thanks to an employee.
- Boarded : vehicle and passenger are on the ship.
- Disembarked : vehicle and passenger has left the ship.
- Canceled : occurs when leg is canceled by the customer or P&O.
- Refunded : Customer has been paid back.

## Customers policy

### Discount

**Business rule 2 :** a customer can have a discount for a given booking which is applied when a booking is paid via the payment service.  
  Ex : if a voyage costs 120 £ and discount rate is 10 % then the customer pays 108 £.

A discount can be uses only once. It is given to a customer and is not linked to any booking at first.  As soon as a customer indicates a discount, it must be linked to the voyage in order to know it has been used.

### Levels

P&O has a VIP customers policy. 

A customer can be :

- Moss : it gives no particular advantages.
- Officer : it gives a 2% discount on all bookings (even if there is a discount). This kind of customer can access the VIP area in ship restaurant.
- Captain : it gives a 5% discount on all bookings (even if there is a discount). This kind of customer can access the VIP area in ship restaurant and is allowed to enter the command area.



