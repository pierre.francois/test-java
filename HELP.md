# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.1/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.1/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

# Some help to code test

## Mocking tests

To mock a test you may follow those steps :

- Adding a test runner :

```java
@RunWith(MockitoJUnitRunner.class)
```

- Declaring mock beans.

<u>Ex :</u>

```java
@Mock
private BookingRepository bookingRepository;
```

- Injecting mocks in the class you test :

```java
@InjectMocks
BookingServiceImpl bookingService;
```

- Telling test runner to instantiate the mock beans.

```java
@BeforeEach
public void init() {
				
	MockitoAnnotations.openMocks(this);
}
```

- In the test method, writing the expectations concerning the mock beans.

<u>Ex :</u>

```java
when(bookingRepository.createVoyageForCustomer(customer, routes)).thenReturn(new Voyage());
```

​       Question : Why returning an instance of *Voyage* is enough here ?

- Calling the method you test.
- Checking mock beans have been called :

<u>Ex :</u>

```java
verify(bookingRepository).createVoyageForCustomer(customer, routes);
```

- This is how to [mock a void method](https://www.baeldung.com/mockito-void-methods).