# Presentation

Presentation should end with a group work :

- Trainees are dispatched in 2/3 student groups.
- Each group draw a [sketchnote](https://apprendre-reviser-memoriser.fr/apprendre-le-sketchnoting/) synthesis of the presentation (10/15 min max).
- Then sketchnotes are compared and trainer checks students have well understood what he has shown and explained.

Note : this is for a better memorization. Students must be aware of that before the presentation starts.

# Trainer's demonstrations

Note : all methods are already implemented.

## Technologies

Implementation : SpringBoot, Spring JPA, Spring MVC.

Test : Junit Jupiter, AssertJ, Mockito, testContainers, MockMvcRequestBuilders and MockMvcResultMatchers APIs.


## Top-down approach

The main aim of the course is to convince students test is a major practice in software engineering to deploy applications that works into production. Students will implement in a top-down manner a whole feature as close as possible to what happens on a project.

So the demonstration shows how to create a component tests suite at API layer level. The feature is "A customer searches routes giving a date".

Class test and tests are written before any implementation to show how to think about design.

Available sources : 

- A model (Ship, Route, Leg, Voyage, Customer)
- ShipRepository and ShipRepositoryJPA.
- ShipService and ShipServiceImpl
- ShipAPI

# End course discussion

At the end of the course, students and trainer may have a discussion concerning some topics related to testing (see last paragraph in README).